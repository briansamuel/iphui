<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ValidationService;
use App\Models\TeamModel;

use DB;

class TeamController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    //
    private $team;
    protected $request;
    protected $validator;

    public function __construct(Request $request, ValidationService $validator, TeamModel $team){
        $this->team = $team;
        $this->request = $request;
        $this->validator = $validator;
    }

    public function index() {
    	
    }
    
    public function getTeamInfo() {
    	// $user = auth()->user();
    	// $user_id = $user['id'];
    	$params = $this->request->only(['id_team']);

    	try {

    		$result = $this->team->findByKey('id', $params['id_team']);
    		if($result) {
    			return response()->json(['status' => 'success','data' => $result]);
    		}

    	} catch(\Exception $e) {

    		return response()->json(['error' => 'Lỗi server quá tải vui lòng thử lại sau', 'code' => $e]);
    	}
    }
    
}
