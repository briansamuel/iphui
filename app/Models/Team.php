<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;

use DB;

class Team extends Model
{
    public function users()
    {
        return $this->hasOne(Voyager::modelClass('User'),  'user_management');
    }
}
