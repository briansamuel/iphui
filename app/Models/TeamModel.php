<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TeamModel
{
    //
    protected static $table = 'teams';

    public static function getMany($limit, $offset, $filter)
    {
        $query = DB::table(self::$table)->skip($offset)->take($limit);
        // if(isset($filter['province']) && $filter['province'] != ""){
        //     $query->join('provinces', 'stores.store_province', '=', 'provinces.id');
        // }
        // if(isset($filter['district']) && $filter['district'] != ""){
        //     $query->join('districts', 'stores.store_district', '=', 'districts.id');
        // }
        // if(isset($filter['ward']) && $filter['ward'] != ""){
        //     $query->join('wards', 'stores.store_ward', '=', 'wards.id');
        // }
        return $query->get();
    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }

    public static function insert($params)
    {
        $insert = DB::table(self::$table)->insertGetId($params);
        return $insert;
    }

    public static function update($id, $params)
    {
        $update = DB::table(self::$table)->where('id', $id)->update($params);
        return $update;
    }


    public static function delete($id)
    {
        $delete = DB::table(self::$table)->where('id', $id)->delete();
        return $delete;
    }
}
